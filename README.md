# Git Shell

_<a href='https://github.com/egordorichev/Git-Shell/raw/master/screenshot.png'>Screenshot</a>_

Git Shell is an interactive shell for git. With it you don't need to type "git ..." each time. Just use commands without 'git' prefix. 

```
git ~/Projects> add ./main.c
git ~/Projects>
```

It's save a lot of time. Hit  <kbd>enter</kbd> to run git status:

```sh
git ~/Projects> ⏎
# On branch master
nothing to commit, working directory clean
git ~/Projects>
```

### Installation

Use GNU Make to build git shell:

```
make
make install
```

Now you can access gitshell:

```
bash ~ $ gitshell
git ~>
```

###### Tricks

* Rename `gitshell` to `gs`. This will save your time.
* If you want to get quotes support, instead of running `make` use `make withsystem`.  

### Build-in commands

* help
* clear
* exit
