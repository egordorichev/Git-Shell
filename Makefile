all:
	gcc src/gitshell.c src/main.c -o gitshell -std=c99 -lreadline -lncurses
withsystem:
	gcc src/gitshell.c src/main.c -o gitshell -std=c99 -lreadline -lncurses -D GITSHELL_USE_SYSTEM
install:
	sudo cp ./gitshell /usr/bin/ 