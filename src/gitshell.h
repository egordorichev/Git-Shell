#ifndef GITSHELL_H_
#define GITSHELL_H_

int gitshell_cd(char **args);
int gitshell_exit(char **args);
int gitshell_help(char **args);
int gitshell_clear(char **args);
char **gitshell_splitline(char *line);
int gitshell_launch(char **args);
int gitshell_execute(char **args);
int gitshell_executewithsystem(char *line);

#endif // GITSHELL_H_
