#include "gitshell.h"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/errno.h>

#include <readline/readline.h>
#include <readline/history.h>
#include <sys/wait.h>
#include <sys/types.h>

const int builtin_functions_total = 4;
char *builtin_functions_names[] = { "cd", "help", "exit", "clear" };
int (*builtin_functions[])(char **) = { &gitshell_cd, &gitshell_help, &gitshell_exit, &gitshell_clear };

int gitshell_cd(char **args){
	if(args[2] == NULL){
		fprintf(stderr, "gitshell: expected argument to \"cd\"\n");
	} else {
		if(chdir(args[2]) != 0){
			perror("gishell");
		}
	}

	return 0;
}

int gitshell_exit(char **args){
	exit(0);
}

int gitshell_help(char **args)
{
	printf("Git shell by Egor Dorichev\n");
	printf("Use git commands like add, commit and status\n");
	printf("Built-in commands: cd, exit, help, clear\n\n");
	
	printf("The most popular git commands are:\n");
	printf("add        Add file contents to the index\n");
	printf("bisect     Find by binary search the change that introduced a bug\n");
	printf("branch     List, create, or delete branches\n");
	printf("checkout   Checkout a branch or paths to the working tree\n");
	printf("clone      Clone a repository into a new directory\n");
	printf("commit     Record changes to the repository\n");
	printf("diff       Show changes between commits, commit and working tree, etc\n");
	printf("fetch      Download objects and refs from another repository\n");
	printf("grep       Print lines matching a pattern\n");
	printf("init       Create an empty Git repository or reinitialize an existing one\n");
	printf("log        Show commit logs\n");
	printf("merge      Join two or more development histories together\n");
	printf("mv         Move or rename a file, a directory, or a symlink\n");
	printf("pull       Fetch from and integrate with another repository or a local branch\n");
	printf("push       Update remote refs along with associated objects\n");
	printf("rebase     Forward-port local commits to the updated upstream head\n");
	printf("reset      Reset current HEAD to the specified state\n");
	printf("rm         Remove files from the working tree and from the index\n");
	printf("show       Show various types of objects\n");
	printf("status     Show the working tree status\n");
	printf("tag        Create, list, delete or verify a tag object signed with GPG\n");

	
	return 0;
}

int gitshell_clear(char **args){
	args[0] = "clear";
	gitshell_launch(args);
}

char **gitshell_splitline(char *line){
	int bufsize = 64;
	int position = 1;
	char **tokens = malloc(bufsize * sizeof(char*));
 	char *token;
	tokens[0] = "git";

	if(!tokens){
	    fprintf(stderr, "gitshell: allocation error\n");
	    exit(EXIT_FAILURE);
	}

 	token = strtok(line, " \t\r\n\a");
 	//char *new_line = readline("> ");
 	
 	while(token != NULL){
		tokens[position] = token;
		position++;

		if(position >= bufsize){
	 		bufsize += 64;
			tokens = realloc(tokens, bufsize * sizeof(char*));
			if(!tokens) {
				fprintf(stderr, "gitshell: allocation error\n");
				exit(EXIT_FAILURE);
			}
		}

		token = strtok(NULL, " \t\r\n\a");
	}
	

	tokens[position] = NULL;
	return tokens;
}

int gitshell_launch(char **args){
	
 	pid_t pid, wpid;
	int status;

	pid = fork();

	if(pid == 0){
		if(execvp(args[0], args) == -1){
			perror("gitshell");
		}
		exit(EXIT_FAILURE);
	} else if (pid < 0){
		perror("gitshell");
	} else {
		do {
			wpid = waitpid(pid, &status, WUNTRACED);
		} while(!WIFEXITED(status) && !WIFSIGNALED(status));
	}

	return 0;
}

int gitshell_execute(char **args){
	if(args[1]){
		for(int i = 0; i < builtin_functions_total; i++){
			if(strcmp(args[1], builtin_functions_names[i]) == 0){
				return (*builtin_functions[i])(args);
			}
		}
		
		return gitshell_launch(args);
	} else {
		args[1] = "status";
		
		return gitshell_launch(args);
	}
}
