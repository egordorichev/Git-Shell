#include "gitshell.h"

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <readline/readline.h>
#include <readline/history.h>

#define RL_PROMPT_START_IGNORE '\001'
#define RL_PROMPT_END_IGNORE '\002'

int main(){
	int status = 0;
	char *line;
	char **args;	
	
	signal(SIGINT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGTTIN, SIG_IGN);
	signal(SIGTTOU, SIG_IGN);
	signal(SIGUSR1, SIG_IGN);
	signal(SIGUSR2, SIG_IGN);

	rl_readline_name = "Gitshell";
	
	using_history();

	do {
		char cwd[1024];
		char promt[1024] = "\001\033[1m\033[32m\002git \001\033[1m\033[34m\002";
		getcwd(cwd, sizeof(cwd));

		if(strstr(cwd, "/home/egor/") != NULL){
			char result[1023];
			strcpy(result, "~");
			strcat(result, cwd + strlen(getenv("HOME")));
			line = readline(strcat(promt, strcat(result, "> \001\033[0m\002")));
		} else {
			line = readline(strcat(promt, strcat(cwd, "> \001\033[0m\002")));
		}

		if(line){
            add_history(line);
			args = gitshell_splitline(line);
			status = gitshell_execute(args);
		}

	} while (status == 0);

	return status;
}
